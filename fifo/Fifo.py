#!/usr/bin/python
# coding=utf-8

import time

class Fifo:

	def scheduling(self, tasks):
		
		endOfTasks = []

		while not tasks.empty():
			# Desenfilera os processos
			task = tasks.dequeue()
			
			print "Processando:", task.PID;

			# espera enquanto o processo é executado
			time.sleep(task.lifeTime)
			# grava no resto dos processos o tempo de espera
			tasks.recordProcess(task.lifeTime)
			# adiciona ao tempo total de processamento do task o restante do tempo
			task.turnaroundTime = task.lifeTime
			# adiciona a task a lista das que já foram executadas
			endOfTasks.append(task)
			print "\tProcesso", task.PID, "processado e encerrado"


		return endOfTasks