#!/usr/bin/python
# coding=utf-8

import random

class Process:
	PID = None
	processType = None
	# Tempo de vida do processo
	lifeTime = None
	# Tempo de vida do processo para backup
	totalLifeTime = None
	# Tempo do processo em espera
	watingTime = 0
	# Tempo do processo em execução
	turnaroundTime = 0
	# Prioridade
	priority = 0


	def __init__(self, pid):
		self.PID = pid
		self.processType = random.randint(1,2)
		self.lifeTime = random.random()*10
		self.totalLifeTime = self.lifeTime
		self.priority = random.randint(1,5)
		# Rodando
		self.state = 1
