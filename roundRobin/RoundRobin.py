#!/usr/bin/python
# coding=utf-8

import threading
import time

class RoundRobin:


	#	Grava informações em outros processos
	def recordProcess(self, tasks, time):
		global lock
		qsize = tasks.qsize()
		
		lock.acquire()
		try:
			for i in range(0,qsize):
				process = tasks.get()
				process.watingTime += time
				process.turnaroundTime += time
				tasks.put(process)
		finally:
			lock.release()

	#	Escalonamento
	def scheduling(self, tasks, quantum):
		global lock
		eoTasks = []
		lock = threading.Lock()
		
		print "\nProcessando via RoundRobin"

		while tasks:

			# Desenfilera os processos
			task = tasks.get()
			
			print "\tProcessando:", task.PID;
		
			if task.lifeTime > quantum:
				# espera enquanto o processo é executado
				time.sleep(quantum)
				# grava no resto dos processos o tempo de espera
				self.recordProcess(tasks, quantum)
				# subtrai o tempo necessário do processo
				task.lifeTime -= quantum
				# adiciona ao tempo total de processamento do task o restante do tempo
				task.turnaroundTime += quantum
				# enfilera o processo novamente
				tasks.put(task)
				print "\tProcesso", task.PID, "processado, tempo restante: ", task.lifeTime
			else:
				# espera enquanto o processo é executado
				time.sleep(task.lifeTime)
				# grava no resto dos processos o tempo de espera
				self.recordProcess(tasks, task.lifeTime)
				# adiciona ao tempo total de processamento do task o restante do tempo
				task.turnaroundTime += task.lifeTime
				# adiciona a task a lista das que já foram executadas
				eoTasks.append(task)
				print "\tProcesso", task.PID, "processado e encerrado"


			if tasks.empty():
				return eoTasks
