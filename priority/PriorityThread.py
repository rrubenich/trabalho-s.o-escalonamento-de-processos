#!/usr/bin/python
# coding=utf-8

import time
import threading
from Queue import Queue

class Priority:



	def processing(self):
		
		global retTasks, elapsed, i
		i = 0

		while True:
			if not retTasks.empty():
				task = retTasks.get()
				# Adiciona o tempo que o processo esperou
				watingTime = elapsed
				# executa todo processo
				task.turnaroundTime = task.lifeTime
				# Adiciona o tempo que levou para processar no total
				elapsed += task.lifeTime
				# Dorme rapaz
				time.sleep(1)
				
				print "\tProcesso:", task.PID, "prioridade", task.priority, "processado"
			else:
				time.sleep(2)
				i+=1
				if i > 10:
					break
				

	def scheduling(self, tasksUnorganized):
		global retTasks, tasks, elapsed

		elapsed = 0

		retTasks = Queue()
		tasks = tasksUnorganized

		threadOrganize = threading.Thread(target = self.organizeProcess)
		threadOrganize.start()

		threadProcess = threading.Thread(target = self.processing)
		threadProcess.start()

		threadOrganize.join()
		threadProcess.join()