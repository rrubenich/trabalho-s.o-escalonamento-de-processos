#!/usr/bin/python
# coding=utf-8

import time

class Priority:

	def scheduling(self, taskList):

		endOfTasks = []
		PPID = 0
		elapsed = 0

		while not taskList.empty():

			tasks = taskList.get()
			PPID += 1

			print "\nProcessando prioridade:", PPID

			while not tasks.empty():

				# Desenfilera os processo
				task = tasks.get()
				print "\tProcessando processo", task.PID
				# Adiciona o tempo que o processo esperou
				task.watingTime = elapsed
				# executa todo processo
				task.turnaroundTime = task.lifeTime
				# Adiciona o tempo que levou para processar no total
				elapsed += task.lifeTime

				time.sleep(task.lifeTime)

				endOfTasks.append(task)

				print "\tProcesso", task.PID, "processado"


		return endOfTasks