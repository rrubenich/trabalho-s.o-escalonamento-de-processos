#!/usr/bin/python
# coding=utf-8

# Transforma uma list em fila #

class Queue:
	
	def __init__(self):
		self.fifo = []
	
	def enqueue(self, item):
		self.fifo.append(item)

	def dequeue(self):
		if not self.empty():
			return self.fifo.pop(0)

	def size(self):
		return len(self.fifo)

	def empty(self):
		return self.size() == 0

	def recordProcess(self, time):
		for process in self.fifo:
			if process.state == 1:
				process.watingTime += time
				process.turnaroundTime += time