#!/usr/bin/python
# coding=utf-8

import roundRobin.RoundRobin
import priority.Priority
import process.Process
import workHandQueue.Queue
import threading
import random
import time
from Queue import Queue


#
#	Imprime os processos após finalizados
#

def printTable(tasks):
	print "\n\n\nPID\tTipo\tVida\tEspera\tTotal de execução"

	for task in tasks:
		if task.processType == 1:
			processType = "I/O"
		else:
			processType = "CPU"

		print task.PID, "\t", processType, "\t", round(task.totalLifeTime, 3), "\t", round(task.watingTime, 3), "\t", round(task.turnaroundTime, 3)

#
#	Cria os processos para RoundRobin
#

def createRoundRobin():
	global tasks, nOfProcess

	for i in range(1,nOfProcess):
		print "Criando o processo:", i
		tasks.put(process.Process.Process(i))
		time.sleep(1)

	print "Todos processos criados"




#
#	Cria os processos para Priority
#

def createPriority():
	global tasks, nOfProcess

	tasksP1 = Queue()
	tasksP2 = Queue()
	tasksP3 = Queue()
	tasksP4 = Queue()
	tasksP5 = Queue()


	for i in range(1, nOfProcess+1):
		task = process.Process.Process(i)
		pri  = task.priority
		print "Criando o processo:", i
		if pri == 1:
			tasksP1.put(task)
		elif pri == 2:
			tasksP2.put(task)
		elif pri == 3:
			tasksP3.put(task)
		elif pri == 4:
			tasksP4.put(task)
		elif pri == 5:
			tasksP5.put(task)


	tasks.put(tasksP1)
	tasks.put(tasksP2)
	tasks.put(tasksP3)
	tasks.put(tasksP4)
	tasks.put(tasksP5)

	print "Todos processos criados"



#
#	Chama RoundRobin
#

def consumeRoundRobin():
	global tasks, quantum, eoTasks

	rr 		= roundRobin.RoundRobin.RoundRobin()
	eoTasks = rr.scheduling(tasks, quantum)

	printTable(eoTasks)





#
#	Chama Priority
#

def consumePriority():
	global tasks, eoTasks

	py 		= priority.Priority.Priority()
	eoTasks = py.scheduling(tasks)

	printTable(eoTasks)








def main():
	global tasks, quantum, eoTasks, nOfProcess

	op 			= 2
	quantum 	= 2
	tasks 		= Queue()
	nOfProcess	= 3

	if op == 1:
		threadProcess = threading.Thread(target = createRoundRobin)
		threadProcess.start()

		threadConsume = threading.Thread(target = consumeRoundRobin)
		threadConsume.start()

		threadProcess.join()
		threadConsume.join()

	elif op == 2:
		createPriority()

		threadConsume = threading.Thread(target = consumePriority)
		threadConsume.start()


		threadConsume.join()



if (__name__ == "__main__"):
	main()
